var app = angular.module('Spotifysearch', ['simplePagination', 'LocalStorageModule']);

			app.config(function($sceDelegateProvider) {
				 $sceDelegateProvider.resourceUrlWhitelist([
				   'self',
				   'https://p.scdn.co/mp3-preview/**']);
			});
			
			app.config(['localStorageServiceProvider', function(localStorageServiceProvider){
			  localStorageServiceProvider.setPrefix('ls');
			}]);
			
			app.factory('Spotify', function Spotify($http) {
				return {
					searchRepos: function searchRepos(query, callback) {
						$http.get('https://api.spotify.com/v1/search?type=artist', { params: { q: query, limit: 50} })
							.success(function (data) {
								callback(null, data);
							})
							.error(function (e) { 
								callback(e);
							});
					},
					getAlbum: function getAlbum(name, callback) {
						$http.get('https://api.spotify.com/v1/search?type=album&limit=50&q='+name) 
							.success(function (data) {
								callback(null, data);
							})
							.error(function (e) {
								callback(e);
						});
					},
					getTracks: function getTracks(name, callback) {
						$http.get('https://api.spotify.com/v1/search?type=track&limit=50&q='+name) 
							.success(function (data) { 
								callback(null, data);
							})
							.error(function (e) {
								callback(e);
						});
					}
				};
			});	
			
			app.controller('SearchController', function SearchController($scope, Spotify,  $sce, Pagination, localStorageService) {				
				$scope.repos      = null;
				$scope.albums     = null;
				$scope.tracks     = null;
				$scope.artistName = null;
				$scope.albumName  = null;
				$scope.executeSearch = function executeSearch() { 
					Spotify.searchRepos($scope.query, function (error, data) { 
						if (!error) { 
							$scope.albums     = null;
							$scope.tracks     = null;
							$scope.artistName = null;
							$scope.albumName  = null;
							$scope.repos = data.artists.items;  
							$scope.pagination = Pagination.artistGetNew(12);
							$scope.pagination.numPages = Math.ceil($scope.repos.length/$scope.pagination.perPage);
						}
					});
				}
					$scope.openAlbum = function openAlbum(name) {
						Spotify.getAlbum(name, function (error, data) {
							if (!error) { 
								$scope.albums = data.albums.items;
								$scope.artistName = name;
								$scope.albumPagination = Pagination.albumGetNew(12);
								$scope.albumPagination.albumNumPages = Math.ceil($scope.albums.length/$scope.albumPagination.albumPerPage);

							}
						});
					}
					$scope.openTrack = function openTrack(name) { 
						Spotify.getTracks(name, function (error, data) {
							if (!error) { 
								$scope.tracks = data.tracks.items; 
								$scope.albumName = name;
								$scope.trackPagination = Pagination.trackGetNew(12);
								$scope.trackPagination.trackNumPages = Math.ceil($scope.tracks.length/$scope.trackPagination.trackPerPage);

							}
						});
					}
					$scope.trusted = function(url) {
						return $sce.trustAsResourceUrl(url);
					}					
					
					/***********Start  Artist Favourite*************/					
					var artistFavInStore = localStorageService.get('artistFavtList');
					
					$scope.artistFavtList = artistFavInStore && artistFavInStore.split('\n') || [];

					$scope.$watch('artistFavtList', function () {
					  localStorageService.add('artistFavtList', $scope.artistFavtList.join('\n'));
					}, true);

					$scope.addFavArtist = function (name) { 
					  $scope.artistFavtList.push(name);					
					};

					$scope.removeFavArtist = function (index) {
					  $scope.artistFavtList.splice(index, 1);
					};	
					/***************Artist End**********************/
					
					/***********Start  Album Favourite*************/					
					var albumFavInStore = localStorageService.get('albumFavtList');
					
					$scope.albumFavtList = albumFavInStore && albumFavInStore.split('\n') || [];

					$scope.$watch('albumFavtList', function () {
					  localStorageService.add('albumFavtList', $scope.albumFavtList.join('\n'));
					}, true);

					$scope.addFavAlbum = function (name) { 
					  $scope.albumFavtList.push(name);					
					};

					$scope.removeFavAlbum = function (index) {
					  $scope.albumFavtList.splice(index, 1);
					};	
					/***************Album End**********************/
					
					/***********Start  Track Favourite*************/					
					var trackFavInStore = localStorageService.get('trackFavtList');
					
					$scope.trackFavtList = trackFavInStore && trackFavInStore.split('\n') || [];

					$scope.$watch('trackFavtList', function () {
					  localStorageService.add('trackFavtList', $scope.trackFavtList.join('\n'));
					}, true);

					$scope.addFavTrack = function (name) { 
					  $scope.trackFavtList.push(name);					
					};

					$scope.removeFavTrack = function (index) {
					  $scope.trackFavtList.splice(index, 1);
					};
					
					/***************Track End**********************/
					
					$(function() {
						naviFun = function(navName){  console.log(navName);
							if(navName =="work"){
							   $("#filters .active").removeClass("active");
							   $("#filters .current").removeClass("current");
							   $("#filters li.work").addClass("active");
							   $("#filters li.work").find("a").addClass("current");
							}	
							if(navName =="album"){
							   $("#filters .active").removeClass("active");
							   $("#filters .current").removeClass("current");
							   $("#filters li.album").addClass("active");
							   $("#filters li.album").find("a").addClass("current");
							}	
							if(navName =="tracks"){
							   $("#filters .active").removeClass("active");
							   $("#filters .current").removeClass("current");
							   $("#filters li.tracks").addClass("active");
							   $("#filters li.tracks").find("a").addClass("current");
							}	
						};
						$("#filters li").click(function(event) { 
						   event.preventDefault();
						   $("#filters .active").removeClass("active");
						   $("#filters .current").removeClass("current");
						   $(this).addClass("active");
						   $(this).find("a").addClass("current");
						   
						   
						   if($.trim($(this).context.innerText)=="Artist")
							 $(location).attr('href', '#work');
						   if($.trim($(this).context.innerText)===$.trim("Album"))	 
						    $(location).attr('href', '#album');
						   if($.trim($(this).context.innerText)===$.trim("Track"))	 
						    $(location).attr('href', '#tracks');						
						});							
					});				
					
			});
			