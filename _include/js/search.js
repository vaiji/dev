var app = angular.module('Spotifysearch', []);
			app.factory('Spotify', function Spotify($http) {
				return {
					searchRepos: function searchRepos(query, callback) {
						$http.get('https://api.spotify.com/v1/search?type=artist', { params: { q: query } })
							.success(function (data) {
								callback(null, data);
							})
							.error(function (e) { 
								callback(e);
							});
					},
					getAlbum: function getAlbum(name, callback) {
						$http.get('https://api.spotify.com/v1/search?type=album&q='+name) 
							.success(function (data) { //console.log(data);
								callback(null, data);
							})
							.error(function (e) {
								callback(e);
						});
					},
					getTracks: function getTracks(name, callback) {
						$http.get('https://api.spotify.com/v1/search?type=track&q='+name) 
							.success(function (data) { //console.log(data);
								callback(null, data);
							})
							.error(function (e) {
								callback(e);
						});
					}
				};
			});
			app.controller('SearchController', function SearchController($scope, Spotify) {
				$scope.executeSearch = function executeSearch() {
					Spotify.searchRepos($scope.query, function (error, data) { 
						if (!error) { 
							$scope.repos = data.artists;
						}
					});
				}
				$scope.openAlbum = function openAlbum(name) {
					Spotify.getAlbum(name, function (error, data) {
						if (!error) { 
							$scope.albums = data.albums;
						}
					});
				}
				$scope.openTrack = function openTrack(name) { console.log(name);
					Spotify.getTracks(name, function (error, data) {
						if (!error) { 
							$scope.tracks = data.tracks; console.log(data.tracks);
						}
					});
				}				
			});
			