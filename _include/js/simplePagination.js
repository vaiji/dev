"use strict";

var paginationModule = angular.module('simplePagination', []);

paginationModule.factory('Pagination', function() {

  var pagination = {};

  pagination.artistGetNew = function(perPage) {

    perPage = perPage === undefined ? 5 : perPage;

    var paginator = {
      numPages: 1,
      perPage: perPage,
      page: 0
    };

    paginator.prevPage = function() {
      if (paginator.page > 0) {
        paginator.page -= 1;
      }
    };

    paginator.nextPage = function() {
      if (paginator.page < paginator.numPages - 1) {
        paginator.page += 1;
      }
    };

    paginator.toPageId = function(id) {
      if (id >= 0 && id <= paginator.numPages - 1) {
        paginator.page = id;
      }
    };

    return paginator;
  };
  
  pagination.albumGetNew = function(albumPerPage) {

    albumPerPage = albumPerPage === undefined ? 5 : albumPerPage;

    var albumPaginator = {
      albumNumPages: 1,
      albumPerPage: albumPerPage,
      page: 0
    };

    albumPaginator.prevPage = function() {
      if (albumPaginator.page > 0) {
        albumPaginator.page -= 1;
      }
    };

    albumPaginator.nextPage = function() {
      if (albumPaginator.page < albumPaginator.albumNumPages - 1) {
        albumPaginator.page += 1;
      }
    };

    albumPaginator.toPageId = function(id) {
      if (id >= 0 && id <= albumPaginator.albumNumPages - 1) {
        albumPaginator.page = id;
      }
    };

    return albumPaginator;
  };
  
  pagination.trackGetNew = function(trackPerPage) {

    trackPerPage = trackPerPage === undefined ? 5 : trackPerPage;

    var trackPaginator = {
      trackNumPages: 1,
      trackPerPage: trackPerPage,
      page: 0
    };

    trackPaginator.prevPage = function() {
      if (trackPaginator.page > 0) {
        trackPaginator.page -= 1;
      }
    };

    trackPaginator.nextPage = function() { 
      if (trackPaginator.page < trackPaginator.trackNumPages - 1) {
        trackPaginator.page += 1;
      }
    };

    trackPaginator.toPageId = function(id) {
      if (id >= 0 && id <= trackPaginator.trackNumPages - 1) {
        trackPaginator.page = id;
      }
    };
    return trackPaginator;
  };
  
  return pagination;
});

paginationModule.filter('startFrom', function() {
  return function(input, start) {
    if (input === undefined) {
      return input;
    } else {
		if(input!=null)
		return input.slice(+start);
    }
  };
});

paginationModule.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);
    for (var i = 0; i < total; i++) {
      input.push(i);
    }
    return input;
  };
});